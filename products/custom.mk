# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/microsoft/mms/products/overlay

PRODUCT_PACKAGE_OVERLAYS += \
    vendor/microsoft/mms/products/overlay/common
